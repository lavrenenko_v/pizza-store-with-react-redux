import React from 'react'
import {Categories, SortPopup, PizzaBlock} from "../components";
import {useDispatch, useSelector} from "react-redux";
import {setCategory, setSortBy} from '../redux/actions/filters'
import {fetchPizzas} from '../redux/actions/pizzas'
import {addPizzaToCart} from '../redux/actions/cart'
import PizzaLoadingBlock from "../components/PizzaBlock/LoadingBlock";

const categoryNames = ['Мясные', 'Вегетарианские', 'Гриль', 'Острые', 'Закрытые']
const sortItems = [
    { name: 'популярности', type: 'popularity', order: 'desc' },
    { name: 'цене', type: 'price', order: 'desc' },
    { name: 'алфавиту', type: 'name', order: 'asc' },
];

function Home({}){
    const items = useSelector(({pizzas})=>pizzas.items)
    const isLoaded = useSelector(({pizzas})=>pizzas.isLoaded)
    const cartItems = useSelector(({cart})=>cart.items)
    const {category, sortBy} = useSelector(({filters})=>filters)
    const dispatch = useDispatch()
    const onSelectCategory = React.useCallback((index)=>{
        dispatch(setCategory(index))
    },[])

    React.useEffect(()=>{
        dispatch(fetchPizzas(category,sortBy))
    }, [category, sortBy])

    const onSelectSortType = React.useCallback((type) => {
        dispatch(setSortBy(type));
    }, []);

    const onAddPizzaToCart = (obj) =>{
        dispatch(addPizzaToCart(obj))
    }


    // default
    // fetchPizzas
    //empty
    //default

    return(
        <div className="container">
            <div className="content__top">
                <Categories activeCategoryIndex={category} onClickCategory={onSelectCategory} items={categoryNames}/>
                <SortPopup activeSortType = {sortBy.type}  items={sortItems} onClickSortType={onSelectSortType}/>
            </div>
            <h2 className="content__title">Все пиццы</h2>
            <div className="content__items">
                {isLoaded?items.map((obj)=><PizzaBlock onClickAddPizza={onAddPizzaToCart}
                                                       addedCount={cartItems[obj.id] && cartItems[obj.id].items.length}
                                                       key={obj.id}{...obj}/>): Array(12).fill(0).map((_, index)=><PizzaLoadingBlock key={index}/>)}
                </div>
            </div>)
}


export default Home;

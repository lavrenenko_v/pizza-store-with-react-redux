import React from 'react'
import PropTypes from "prop-types";

// class Categories extends React.Component{
//     state = {
//         activeItem:3
//     };
//
//     onSelectItem = index => {
//         this.setState(
//             {
//                 activeItem: index
//             }
//         );
//     }
//
//
//     render(){
//         const {items, onClick}= this.props
//         return(
//             <div className="categories">
//                 <ul>
//                     <li className="active">Все</li>
//                     {
//                         items.map((item, index)=>(
//                             <li className={this.state.activeItem===index? 'active': '' } onClick={()=>this.onSelectItem(index)} key={`${item}_${index}`}>{item}</li>))
//                     }
//                 </ul>
//             </div>
//         )
//     }
// }
//



const Categories = React.memo(function Categories({activeCategoryIndex, onClickCategory, items}){

    return(
        <div className="categories">
            <ul>
                <li className={activeCategoryIndex === null ? 'active' : ''} onClick={()=>onClickCategory(null)}>Все</li>
                {
                    items && items.map((item, index)=>(
                        <li className={activeCategoryIndex === index ? 'active' : ''} onClick={()=>onClickCategory(index)} key={`${item}_${index}`}>{item}</li>))
                }
            </ul>
        </div>
    )
})


Categories.propTypes = {
    activeCategoryIndex:PropTypes.oneOfType([PropTypes.number, PropTypes.oneOf([null])]),
    items:PropTypes.arrayOf(PropTypes.string).isRequired,
    onClick:PropTypes.func
}

Categories.defaultProps = {
    activeCategoryIndex:null,
    items:[]
}

export default Categories;


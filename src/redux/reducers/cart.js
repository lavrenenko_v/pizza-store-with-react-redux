const initialState = {
    items:{},
    totalPrice:0,
    totalCount:0
}
const _get = (obj, path) => {
    const [firstKey, ...keys] = path.split('.');
    return keys.reduce((val, key) => {
        return val[key];
    }, obj[firstKey]);
};

const getTotalSum = (obj, path) => {
    return Object.values(obj).reduce((sum, obj) => {
        const value = _get(obj, path);
        return sum + value;
    }, 0);
};
const getTotalPrice = arr => arr.reduce((sum, obj) => obj.price + sum, 0)
const getCountById = arr => arr?arr.length:0



const cartReducer=(state=initialState, action)=>{
    switch (action.type){

        case 'ADD_PIZZA_CART':{
            const currentPizzasItems = !state.items[action.payload.id]?
                [action.payload]:[...state.items[action.payload.id].items, action.payload]
            const newItems = {
                ...state.items,
                [action.payload.id]: {
                    items:currentPizzasItems,
                    price:getTotalPrice(currentPizzasItems),
                    count:getCountById(currentPizzasItems)
            }
            }

            const totalCount = getTotalSum(newItems, 'items.length')
            const totalPrice = getTotalSum(newItems, 'price')

            return{
                ...state,
                items:newItems,
                totalCount,
                totalPrice
                    // 1. create a new object
                    // 2. take the old items by id passed into action
                    // 3. add payload to the new items
                }
        }
        case 'CLEAR_CART':{
            return initialState
        }

        case 'REMOVE_CART_ITEM':{
            const newItems ={
                ...state.items
            }
            const currentTotalPrice = newItems[action.payload].price
            const currentTotalCount = newItems[action.payload].count
            delete newItems[action.payload]
            return {
                ...state,
                items: newItems,
                totalPrice: state.totalPrice - currentTotalPrice,
                totalCount: state.totalCount - currentTotalCount
            }
        }

        case 'PLUS_CART_ITEM':{
            const newObjItems = [
                ...state.items[action.payload].items,
                state.items[action.payload].items[0],
            ];

            const newItems = {
                ...state.items,
                [action.payload]: {
                    items: newObjItems,
                    price: getTotalPrice(newObjItems),
                    count: getCountById(newObjItems)
                },
            };

            const totalCount = getTotalSum(newItems, 'items.length');
            const totalPrice = getTotalSum(newItems, 'price');

            return {
                ...state,
                items: newItems,
                totalPrice,
                totalCount,
            };
        }

        case 'MINUS_CART_ITEM':{
            const oldItems = state.items[action.payload].items;
            const newObjItems =
                oldItems.length > 1 ? state.items[action.payload].items.slice(1) : oldItems;
            const newItems = {
                ...state.items,
                [action.payload]: {
                    items: newObjItems,
                    price: getTotalPrice(newObjItems),
                    count: getCountById(newObjItems)
                },
            };

            const totalCount = getTotalSum(newItems, 'items.length');
            const totalPrice = getTotalSum(newItems, 'price');

            return {
                ...state,
                items: newItems,
                totalPrice,
                totalCount
            };
        }
        case 'SET_TOTAL_PRICE':{
            return {
                ...state,
                totalPrice: action.payload
            }
        }
        case 'SET_TOTAL_COUNT':{
            return {
                ...state,
                totalCount: action.payload
            }
        }

    }
    return state
}

export default cartReducer;



